FROM python:3 AS builder

RUN useradd scrapy && mkdir /app && chown scrapy /app
USER scrapy
COPY requirements.txt Makefile scrapy.cfg /app/
WORKDIR /app
RUN make venv
COPY immospider /app/immospider
COPY jinja2 /app/jinja2

FROM python:3-slim
RUN useradd scrapy && mkdir /app && chown scrapy /app
USER scrapy
COPY --from=builder /app /app
WORKDIR /app
CMD venv/bin/scrapy crawl immoscout -o /output/apartments.csv -a dest=$DEST -a url=$URL -L INFO
