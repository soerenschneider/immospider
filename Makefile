.PHONY: venv

run:
	podman run -v scrappy:/output -e URL="${URL}" immo

run-native:
	rm -f apartments.json
	venv/bin/scrapy crawl immoscout -o apartments.json -a dest="${DEST}" -a bot_user="${BOT_USER}" -a bot_token="${BOT_TOKEN}" -a url="${URL}" -L INFO

venv:
	python3 -m venv venv
	venv/bin/pip3 install -r requirements.txt

docker:
	podman build -t immo .
