import logging
import os
from jinja2 import Environment, FileSystemLoader
from scrapy import signals
import telegram

logger = logging.getLogger(__name__)

class Notification:
	def __init__(self):
		self.items = list()

	def spider_opened(self, spider):
		self.user = spider.bot_user

		token = spider.bot_token
		self.bot = telegram.Bot(token=token)

	@classmethod
	def from_crawler(cls, crawler):
		ext = cls()

		crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
		crawler.signals.connect(ext.spider_closed, signal=signals.spider_closed)
		crawler.signals.connect(ext.item_scraped, signal=signals.item_scraped)

		return ext
	
	def item_scraped(self, item, spider):
		self.items.append(item)

	def spider_closed(self, spider):
		if len(self.items) > 0:
			self.render(f"We have {len(self.items)} new items")

	def render(self, text):
		self.bot.send_message(chat_id=self.user, text=text)

class Table:
	def __init__(self, dest="/tmp"):
		self.items = list()
		self.dest = dest

	@classmethod
	def from_crawler(cls, crawler):
		ext = cls()

		crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
		crawler.signals.connect(ext.spider_closed, signal=signals.spider_closed)
		crawler.signals.connect(ext.item_scraped, signal=signals.item_scraped)

		return ext
	
	def item_scraped(self, item, spider):
		self.items.append(item)

	def spider_opened(self, spider):
		self.dest = spider.dest

	def spider_closed(self, spider):
		if len(self.items) > 0:
			self.render()

	def render(self):
		file_loader = FileSystemLoader('jinja2')
		env = Environment(loader=file_loader)
		template = env.get_template("table.html")

		sorted(self.items, key=lambda item: item["date"], reverse=True)
		html = template.render(title="Email from scraper", items=self.items)
		dest = os.path.join(self.dest, "index.html")
		with open(dest, "wb") as f:
			f.write(html.encode(encoding='UTF-8'))